/**********************************************************
 * Control Room
 **********************************************************/

@base: #97ADBC;
@land: #EEDDC8;
@motorway: #fbfaf7;
@motorway-border: #C5C6C6;
@street: #fbfaf7;
@gemeentegrens: #FFF;
@parks: #c4d9c4;
@buildings: #FFFCE6;

Map { background-color:@base; }
#national_borders { polygon-fill:@land; }

#nederland_grens {
  line-color:#0AF;
  line-dasharray:1,1;
  line-width:0.5;
  line-opacity:0.5;
  [zoom=4] { line-width:0.6; }
  [zoom=5] { line-width:0.8; }
  [zoom=6] { line-width:1; }
  [zoom=7] { line-width:1.2; }
  [zoom=8] { line-width:1.4; }
  [zoom>8] { line-width:1.6; }
}

#nederland_grens::glow-inner[zoom>7] {
  line-color:#225160;
  line-width:1.2;
  line-join:round;
  line-opacity:0.4;
}
#nederland_grens::glow-innermiddle[zoom>7] {
  line-color:#225160;
  line-width:2.5;
  line-join:round;
  line-opacity:0.2;
}
#nederland_grens::glow-outermiddle[zoom>7] {
  line-color:#225160;
  line-width:5;
  line-join:round;
  line-opacity:0.1;
}
#nederland_grens::glow-outer[zoom>7] {
  line-color:#225160;
  line-width:5;
  line-join:round;
  line-opacity:0.05;
}


#water[zoom>7] {
  polygon-fill:@base;
}

#parkforest[zoom>11] {
  polygon-opacity:0.5;
  polygon-fill:@parks;
}

#buildings[zoom>7] {
  polygon-fill:@buildings;
}

#roads {
  [TYPE='trunk'], [TYPE='trunk_link'], [TYPE='motorway'], [TYPE='primary'] {
    ::case { line-color: @motorway-border; line-width:0; }
    ::fill { line-color: @motorway; line-width:0; }
    line-width:0;
  }
  [TYPE='secondary'], [TYPE='service'], [TYPE='residential'], [TYPE='unclassified'], [TYPE='living street'] {
    line-color: @street;
    line-width: 0;
  }  
}
#roads[zoom=7], #roads[zoom=8] {
  [TYPE='trunk'], [TYPE='trunk_link'], [TYPE='motorway'] {
    ::case { line-width:1; line-opacity:0.3; }
    ::fill { line-width:1; line-opacity:0.3; }
  }
}
#roads[zoom=9] {
  [TYPE='trunk'], [TYPE='trunk_link'], [TYPE='motorway'] {
    ::case { line-width:1.5; line-opacity:0.5; }
    ::fill { line-width:1; line-opacity:0.5; }
  }
}
#roads[zoom=10] {
  [TYPE='trunk'], [TYPE='trunk_link'], [TYPE='motorway'] {
    ::case { line-width:1.5; line-opacity:0.5; }
    ::fill { line-width:1; line-opacity:0.5; }
  }
}
#roads[zoom=11], #roads[zoom=12] {
  [TYPE='trunk'], [TYPE='trunk_link'], [TYPE='motorway'], [TYPE='primary'] {
    ::case { line-width:2; }
    ::fill { line-width:1; }
  }
  [TYPE='primary'] {
    ::case { line-width:1.5; line-opacity:0.5; }
    ::fill { line-width:1; line-opacity:0.5; }
  }
}   
#roads[zoom=13] {
  [TYPE='trunk'], [TYPE='trunk_link'], [TYPE='motorway'] {
    ::case { line-width:2; }
    ::fill { line-width:1; }
  }
  [TYPE='primary'] {
    ::case { line-width:1.5; }
    ::fill { line-width:1; }
  }
  
}
#roads[zoom=14] {
  [TYPE='trunk'], [TYPE='trunk_link'], [TYPE='motorway'] {
    ::case { line-width:2; }
    ::fill { line-width:1; }
  }
  [TYPE='primary'] {
    ::case { line-width:1.5; }
    ::fill { line-width:1; }
  }
  [TYPE='service'], [TYPE='secondary'], [TYPE='residential'], [TYPE='unclassified'],
  [TYPE='living street'] {
    line-width:1.2;
    line-opacity:0.5;
  }
}
#roads[zoom=15] {
  [TYPE='trunk'], [TYPE='trunk_link'], [TYPE='motorway'] {
    ::case { line-width:4; }
    ::fill { line-width:2; }
  }
  [TYPE='primary'] {
    ::case { line-width:2; }
    ::fill { line-width:1; }
  }
  [TYPE='secondary'] {
    line-width:3;
    line-opacity:0.5;
  }
  [TYPE='service'], [TYPE='residential'], [TYPE='unclassified'], [TYPE='living street'] {
    line-width:1.5;
    line-opacity:0.5;
  }
}



@l1: #61B148;
@l2: #90C149;
@l3: #D6D951;
@l4: #F97B42;
@l5: #F23837;


#energiegebruikgemeenten[zoom<8] {
  line-width: 0;
}



#energiegebruikgemeenten[zoom<=10] {
  [RECS='Gemeente'] {
    line-color: #333;
    line-width: 0.6;
    line-dasharray:2,2;
  }
  [RECS='Gemeente'][el_2009<3250] {
    polygon-opacity:0.9;
    polygon-fill: @l1;
  }
  [RECS='Gemeente'][el_2009>=3250][el_2009<3500] {
    polygon-opacity:0.9;
    polygon-fill: @l2;
  }
  [RECS='Gemeente'][el_2009>=3500][el_2009<3700] {
    polygon-opacity:0.9;
    polygon-fill: @l3;
  }
  [RECS='Gemeente'][el_2009>=3700][el_2009<3850] {
    polygon-opacity:0.9;
    polygon-fill: @l4;
  }
  [RECS='Gemeente'][el_2009>=3850] {
    polygon-opacity:0.9;
    polygon-fill: @l5;
  }
}

#energiegebruikgemeenten[zoom>10][zoom<13] {
  [RECS='Wijk'] {
    line-width: 1.5;
    line-color: @gemeentegrens;
    line-dasharray: 4,4;
  }
  [RECS='Wijk'][el_2009<3250] {
    polygon-opacity:0.9;
    polygon-fill: @l1;
  }
  [RECS='Wijk'][el_2009>=3250][el_2009<3500] {
    polygon-opacity:0.9;
    polygon-fill: @l2;
  }
  [RECS='Wijk'][el_2009>=3500][el_2009<3700] {
    polygon-opacity:0.9;
    polygon-fill: @l3;
  }
  [RECS='Wijk'][el_2009>=3700][el_2009<3850] {
    polygon-opacity:0.9;
    polygon-fill: @l4;
  }
  [RECS='Wijk'][el_2009>=3850] {
    polygon-opacity:0.9;
    polygon-fill: @l5;
  }
  [RECS='Wijk'][el_2009=0] {
    polygon-opacity:0.9;
    polygon-fill: @l1;
  }
}

#energiegebruikgemeenten[zoom>=13] {
  [RECS='Buurt'] {
    line-color: @gemeentegrens;
    line-width: 3;
    line-dasharray: 4,4;
  }
  [RECS='Buurt'][el_2009<2900] {
    polygon-opacity:0.9;
    polygon-fill: @l1;
  }
  [RECS='Buurt'][el_2009>=2900][el_2009<3500] {
    polygon-opacity:0.9;
    polygon-fill: @l2;
  }
  [RECS='Buurt'][el_2009>=3500][el_2009<4000] {
    polygon-opacity:0.9;
    polygon-fill: @l3;
  }
  [RECS='Buurt'][el_2009>=4000][el_2009<4600] {
    polygon-opacity:0.9;
    polygon-fill: @l4;
  }
  [RECS='Buurt'][el_2009>=4600] {
    polygon-opacity:0.9;
    polygon-fill: @l5;
  }
  [RECS='Buurt'][el_2009=0] {
    polygon-opacity:0.9;
    polygon-fill: @l1;
  }
}
    
    
    
    
    
    
    
    
    
    




















