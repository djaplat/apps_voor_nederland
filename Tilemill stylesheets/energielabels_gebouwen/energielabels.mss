#energielabels_gebouwen {
  marker-allow-overlap: true;
  marker-height:0;
  marker-line-width:0;
}


@Z10: #FFF;
@G11: #FC8D59; @G13: #D73027;
@F13: #FEE08B;
@E13: #FFFFBF;
@D13: #FFFFFF;
@C13: #FFFFBF;
@B13: #D9EF8B;
@A13: #1A9850;

#energielabels_gebouwen[zoom=8] {
  marker-line-color:@Z10; marker-fill:@Z10;
  marker-opacity: .01; marker-line-opacity: .01;
  marker-height: .5; marker-line-width: 0.4;
  [en_klasse='G'], [en_klasse='F'], [en_klasse='E'] {
    marker-opacity: .04; marker-line-opacity: .04;
  }
  [en_klasse='G'] {
    marker-fill:@G13; marker-line-color:@G13;
  }
  [en_klasse='A'] {
    marker-fill:@A13; marker-line-color:@A13;
    marker-opacity: .04; marker-line-opacity: .04;
  }
}

#energielabels_gebouwen[zoom=9] {
  marker-line-color:@Z10; marker-fill:@Z10;
  marker-opacity: .02; marker-line-opacity: .01;
  marker-height: .6; marker-line-width: 0.5;
  [en_klasse='G'], [en_klasse='F'], [en_klasse='E'] {
    marker-opacity: .04; marker-line-opacity: .04;
  }
  [en_klasse='G'] {
    marker-fill:@G13; marker-line-color:@G13;
  }
  [en_klasse='A'] {
    marker-fill:@A13; marker-line-color:@A13;
    marker-opacity: .04; marker-line-opacity: .04;
  }
}
#energielabels_gebouwen[zoom=10] {
  marker-line-color:@Z10; marker-fill:@Z10;
  marker-opacity: .03; marker-line-opacity: .02;
  marker-height: .8; marker-line-width: 0.1;
  [en_klasse='G'], [en_klasse='F'], [en_klasse='E'] {
    marker-opacity: .06; marker-line-opacity: .06;
  }
  [en_klasse='G'] {
    marker-fill:@G13; marker-line-color:@G13;
  }
  [en_klasse='E'] {
    marker-fill:@E13; marker-line-color:@E13;
    marker-opacity: .03; marker-line-opacity: .02;
  }
  [en_klasse='B'] {
    marker-fill:@B13; marker-line-color:@B13;
    marker-opacity: .03; marker-line-opacity: .02;
  }
  [en_klasse='A'] {
    marker-fill:@A13; marker-line-color:@A13;
    marker-opacity: .06; marker-line-opacity: .06;
  }
}
#energielabels_gebouwen[zoom=11] {
  marker-line-color:#FFF; marker-fill:#FFF;
  marker-opacity: .05; marker-line-opacity: .03;
  marker-height: .9; marker-line-width: 0.3;
  [en_klasse='G'], [en_klasse='F'] {
    marker-fill:@G13; marker-line-color:@G13;
    marker-opacity: .06; marker-line-opacity: .06;
  }
  [en_klasse='E'] {
    marker-fill:@E13; marker-line-color:@E13;
    marker-opacity: .05; marker-line-opacity: .03;
  }
  [en_klasse='B'] {
    marker-fill:@B13; marker-line-color:@B13;
    marker-opacity: .05; marker-line-opacity: .03;
  }
  [en_klasse='A'] {
    marker-fill:@A13; marker-line-color:@A13;
    marker-opacity: .06; marker-line-opacity: .06;
  }
}
#energielabels_gebouwen[zoom=12] {
  marker-line-color:#FFF; marker-fill:#FFF;
  marker-opacity: .05; marker-line-opacity: .03;
  marker-height: 1; marker-line-width: 0.5;
  [en_klasse='G'], [en_klasse='F'] {
    marker-fill:@G13; marker-line-color:@G13;
    marker-opacity: .06; marker-line-opacity: .06;
  }
  [en_klasse='E'] {
    marker-fill:@E13; marker-line-color:@E13;
    marker-opacity: .06; marker-line-opacity: .06;
  }
  [en_klasse='B'] {
    marker-fill:@B13; marker-line-color:@B13;
    marker-opacity: .06; marker-line-opacity: .06;
  }
  [en_klasse='A'] {
    marker-fill:@A13; marker-line-color:@A13;
    marker-opacity: .06; marker-line-opacity: .06;
  }
}

#energielabels_gebouwen[zoom=13] {
  marker-height:1; marker-line-width:0.7;
  [en_klasse='G'] {
    marker-line-color: @G13; marker-fill: @G13;
    marker-opacity:.5; marker-line-opacity:.3;    
  }  
  [en_klasse='F'] {
    marker-fill: @F13; marker-fill: @F13;
    marker-opacity:.3; marker-line-opacity:.2;
  }  
  [en_klasse='E'] {
    marker-line-color: @E13; marker-fill: @E13;
    marker-opacity: .15; marker-line-opacity: .07; 
  }
  [en_klasse='D'] {
    marker-line-color: @D13; marker-fill: @D13;
    marker-opacity: .15; marker-line-opacity: .07; 
  }
  [en_klasse='C'] {
    marker-line-color: @C13; marker-fill: @C13;
    marker-opacity: .15; marker-line-opacity: .07; 
  }
  [en_klasse='B'] {
    marker-line-color: @B13; marker-fill: @B13;
    marker-opacity: .3; marker-line-opacity: .2; 
  }
  [en_klasse='A'] {
    marker-line-color: @A13; marker-fill: @A13;
    marker-opacity: .5; marker-line-opacity: .3;
  }
}

#energielabels_gebouwen[zoom=14] {
  marker-height:1.3; marker-line-width:1;
  [en_klasse='G'] {
    marker-line-color: @G13; marker-fill: @G13;
    marker-opacity:.55; marker-line-opacity:.3;    
  }  
  [en_klasse='F'] {
    marker-fill: @F13; marker-fill: @F13;
    marker-opacity:.35; marker-line-opacity:.2;
  }  
  [en_klasse='E'] {
    marker-line-color: @E13; marker-fill: @E13;
    marker-opacity: .2; marker-line-opacity: .07; 
  }
  [en_klasse='D'] {
    marker-line-color: @D13; marker-fill: @D13;
    marker-opacity: .2; marker-line-opacity: .07; 
  }
  [en_klasse='C'] {
    marker-line-color: @C13; marker-fill: @C13;
    marker-opacity: .2; marker-line-opacity: .07; 
  }
  [en_klasse='B'] {
    marker-line-color: @B13; marker-fill: @B13;
    marker-opacity: .35; marker-line-opacity: .2; 
  }
  [en_klasse='A'] {
    marker-line-color: @A13; marker-fill: @A13;
    marker-opacity: .55; marker-line-opacity: .3;
  }
}

#energielabels_gebouwen[zoom=15] {
  marker-height:1.8; marker-line-width:1;
  [en_klasse='G'] {
    marker-line-color: @G13; marker-fill: @G13;
    marker-opacity:.8; marker-line-opacity:.8;
  }  
  [en_klasse='F'] {
    marker-fill: @F13; marker-fill: @F13;
    marker-opacity:.5; marker-line-opacity:.3;
  }  
  [en_klasse='E'] {
    marker-line-color: @E13; marker-fill: @E13;
    marker-opacity: .3; marker-line-opacity: .1; 
  }
  [en_klasse='D'] {
    marker-line-color: @D13; marker-fill: @D13;
    marker-opacity: .2; marker-line-opacity: .1; 
  }
  [en_klasse='C'] {
    marker-line-color: @C13; marker-fill: @C13;
    marker-opacity: .3; marker-line-opacity: .1; 
  }
  [en_klasse='B'] {
    marker-line-color: @B13; marker-fill: @B13;
    marker-opacity: .5; marker-line-opacity: .3; 
  }
  [en_klasse='A'] {
    marker-line-color: @A13; marker-fill: @A13;
    marker-opacity: .8; marker-line-opacity: .6;
  }
}

