/**********************************************************
 * Control Room
 **********************************************************/

@base: #000D12;
@land: #0A202A;
@motorway: #07171E;
@motorway-border: #004769;
@street: #021019;
@gemeentegrens: #4C6469;
@parks: #0a242e;
@buildings: #010a0f;

Map { 
  [zoom>9] {
    background-color:@base; 
  }
}

#nederland_grens {
  line-color:#0AF;
  line-dasharray:1,1;
  line-width:0.5;
  line-opacity:0.5;
  [zoom=4] { line-width:0.6; }
  [zoom=5] { line-width:0.8; }
  [zoom=6] { line-width:1; }
  [zoom=7] { line-width:1.2; }
  [zoom=8] { line-width:1.4; }
  [zoom>8] { line-width:1.6; }
}

#national_borders::glow-inner[zoom>8] {
  line-color:#225160;
  line-width:1.2;
  line-join:round;
  line-opacity:0.4;
}
#national_borders::glow-innermiddle[zoom>8] {
  line-color:#225160;
  line-width:2.5;
  line-join:round;
  line-opacity:0.2;
}
#national_borders::glow-outermiddle[zoom>8] {
  line-color:#225160;
  line-width:5;
  line-join:round;
  line-opacity:0.1;
}
#national_borders::glow-outer[zoom>8] {
  line-color:#225160;
  line-width:5;
  line-join:round;
  line-opacity:0.05;
}
#national_borders[zoom>=9] {
  polygon-fill:@land;
  polygon-gamma:1;
}

#rivers[zoom>7] {
  polygon-fill:@base;
}

/*
#rivers[zoom<11][NAME=''] {
  polygon-opacity:0;
}
*/
#parkforest[zoom>11] {
  line-color:@parks;
  line-width:0.5;
  polygon-opacity:1;
  polygon-fill:@parks;
}

#buildings[zoom>12] {
  polygon-fill:@buildings;
}


#roads {
  [TYPE='trunk'], [TYPE='trunk_link'], [TYPE='motorway'], [TYPE='primary'] {
    ::case { line-color: @motorway-border; line-width:0; }
    ::fill { line-color: @motorway; line-width:0; }
    line-width:0;
  }
  [TYPE='secondary'], [TYPE='service'], [TYPE='residential'], [TYPE='unclassified'], [TYPE='living street'] {
    line-color: @base;
    line-width: 0;
  }  
}
#roads[zoom=7], #roads[zoom=8] {
  [TYPE='trunk'], [TYPE='trunk_link'], [TYPE='motorway'] {
    ::case { line-width:1; }
    ::fill { line-width:1; }
  }
}
#roads[zoom=9] {
  [TYPE='trunk'], [TYPE='trunk_link'], [TYPE='motorway'] {
    ::case { line-width:1.5; }
    ::fill { line-width:1; }
  }
}
#roads[zoom=10] {
  [TYPE='trunk'], [TYPE='trunk_link'], [TYPE='motorway'] {
    ::case { line-width:1.5; }
    ::fill { line-width:1; }
  }
}
#roads[zoom=11], #roads[zoom=12] {
  [TYPE='trunk'], [TYPE='trunk_link'], [TYPE='motorway'], [TYPE='primary'] {
    ::case { line-width:2; }
    ::fill { line-width:1; }
  }
  [TYPE='primary'] {
    ::case { line-width:1.5; }
    ::fill { line-width:1; }
  }
  [TYPE='service'], [TYPE='secondary'], [TYPE='residential'], [TYPE='unclassified'],
  [TYPE='living street'] {
    line-width:1;
    line-color:@motorway;
  }
}   
#roads[zoom=13] {
  [TYPE='trunk'], [TYPE='trunk_link'], [TYPE='motorway'] {
    ::case { line-width:2; }
    ::fill { line-width:1; }
  }
  [TYPE='primary'] {
    ::case { line-width:1.5; }
    ::fill { line-width:1; }
  }
  [TYPE='service'], [TYPE='secondary'], [TYPE='unclassified'] {
    line-width:1;
  }
}
#roads[zoom=14] {
  [TYPE='trunk'], [TYPE='trunk_link'], [TYPE='motorway'] {
    ::case { line-width:2; }
    ::fill { line-width:1; }
  }
  [TYPE='primary'] {
    ::case { line-width:1.5; }
    ::fill { line-width:1; }
  }
  [TYPE='service'], [TYPE='secondary'], [TYPE='residential'], [TYPE='unclassified'],
  [TYPE='living street'] {
    line-width:1.2;
  }
}
#roads[zoom=15] {
  [TYPE='trunk'], [TYPE='trunk_link'], [TYPE='motorway'] {
    ::case { line-width:4; }
    ::fill { line-width:2; }
  }
  [TYPE='primary'] {
    ::case { line-width:2; }
    ::fill { line-width:1; }
  }
  [TYPE='secondary'] {
    line-width:3;
  }
  [TYPE='service'], [TYPE='residential'], [TYPE='unclassified'], [TYPE='living street'] {
    line-width:1.5;
  }
}
    
    
@l1: #FCFE34;
@l2: #FEB022;
@l3: #FF680F;
@l4: #FF2800;
@l5: #555;


    
    
    




















