@inner: #f45;
@outer: #FFF;
@text: #FFF;
@halo: #000;

@sans:"Droid Sans Book","Arial Regular","DejaVu Sans Book";
@sans_bold:"Droid Sans Bold","Arial Bold","DejaVu Sans Bold";


#countries {
  ::outline {
    line-color: #85c5d3;
    line-width: 2;
    line-join: round;
  }
  polygon-fill: #fff;
}

#hoogsteenergie[zoom=7][el_round>150000] {
  marker-fill:@inner;
  marker-line-color:@outer;
  marker-allow-overlap:false;
  text-name:"[el_round]";
  text-face-name: @sans;
  text-fill: @text;
  text-allow-overlap:false;
  text-size:9;
  text-dy:-2;
  [el_round<=2528936] {
    marker-width:5; marker-line-width:3; text-dy:-6;
  }
  [el_round<250000] {
    marker-width:1; marker-line-width:3; text-dy:-2;
  }  
  [el_round>2528936] {
    marker-width:10; marker-line-width:4; text-dy:-10; text-size:10;
  } 
}

#hoogsteenergie[zoom=8][el_round>130000] {
  marker-fill:@inner;
  marker-line-color:@outer;
  marker-allow-overlap:false;
  text-name:"[el_round]";
  text-face-name: @sans;
  text-fill: @text;
  text-allow-overlap:false;
  text-size:9;
  text-dy:-2;
  [verbr_el<=2528936] {
    marker-width:10; marker-line-width:4; text-dy:-11;
  }  
  [verbr_el<430383] {
    marker-width:7; marker-line-width:3; text-dy: -8;
  }
  [verbr_el<200217] {
    marker-width:3; marker-line-width:2; text-dy:-4;
  }  
  [verbr_el>2528936] {
    marker-width:20; marker-line-width:5; text-dy:-21;
  }
}

#hoogsteenergie[zoom=9][el_round>100000] {
  marker-fill:@inner;
  marker-line-color:@outer;
  marker-allow-overlap:false;
  text-name:"[el_round]";
  text-face-name: @sans;
  text-fill: @text;
  text-allow-overlap:false;
  text-size:9;
  text-dy:-2;
  [verbr_el<=2528936] {
    marker-width:12; marker-line-width:5; text-dy:-11;
  }  
  [verbr_el<430383] {
    marker-width:8; marker-line-width:4; text-dy: -8;
  }
  [verbr_el<200217] {
    marker-width:3; marker-line-width:2; text-dy:-4;
  }  
  [verbr_el>2528936] {
    marker-width:25; marker-line-width:7; text-dy:-26;
  }
}

#hoogsteenergie[zoom=10][el_round>80000] {
  marker-fill:@inner;
  marker-line-color:@outer;
  marker-allow-overlap:false;
  text-name:"[el_round]";
  text-face-name: @sans;
  text-fill: @text;
  text-allow-overlap:false;
  text-size:10;
  text-dy:-2;
  [verbr_el<=2528936] {
    marker-width:15; marker-line-width:6; text-dy:-16;
  }  
  [verbr_el<430383] {
    marker-width:10; marker-line-width:4; text-dy: -11;
  }
  [verbr_el<200217] {
    marker-width:3; marker-line-width:2; text-dy:-4;
  }  
  [verbr_el>2528936] {
    marker-width:30; marker-line-width:8; text-dy:-31;
  }
}

#hoogsteenergie[zoom=11][el_round>50000] {
  marker-fill:@inner;
  marker-line-color:@outer;
  marker-allow-overlap:false;
  text-name:"[el_round]";
  text-face-name: @sans;
  text-fill: @text;
  text-allow-overlap:false;
  text-size:11;
  text-dy:-2;
  [verbr_el<=2528936] {
    marker-width:20; marker-line-width:7; text-dy:-21;
  }  
  [verbr_el<430383] {
    marker-width:14; marker-line-width:5; text-dy: -15;
  }
  [verbr_el<200217] {
    marker-width:6; marker-line-width:3; text-dy:-7;
  }  
  [verbr_el>2528936] {
    marker-width:40; marker-line-width:10; text-dy:-41;
  }
}

#hoogsteenergie[zoom=11][el_round>20000] {
  marker-fill:@inner;
  marker-line-color:@outer;
  marker-allow-overlap:false;
  text-name:"[el_round]";
  text-face-name: @sans;
  text-fill: @text;
  text-allow-overlap:false;
  text-size:11;
  text-dy:-2;
  [verbr_el<=2528936] {
    marker-width:24; marker-line-width:8; text-dy:-25;
  }  
  [verbr_el<430383] {
    marker-width:18; marker-line-width:6; text-dy: -19;
  }
  [verbr_el<200217] {
    marker-width:10; marker-line-width:4; text-dy:-11;
  } 
  [verbr_el<40383] {
    marker-width:6; marker-line-width:3; text-dy: -7;
  }
  [verbr_el>2528936] {
    marker-width:40; marker-line-width:10; text-dy:-41;
  }
}

#hoogsteenergie[zoom=12][el_round>10000] {
  marker-fill:@inner;
  marker-line-color:@outer;
  marker-allow-overlap:false;
  text-name:"[el_round]";
  text-face-name: @sans;
  text-fill: @text;
  text-allow-overlap:false;
  text-size:11;
  text-dy:-2;
  [verbr_el<=2528936] {
    marker-width:24; marker-line-width:8; text-dy:-25;
  }  
  [verbr_el<430383] {
    marker-width:18; marker-line-width:6; text-dy: -19;
  }
  [verbr_el<200217] {
    marker-width:10; marker-line-width:4; text-dy:-11;
  } 
  [verbr_el<40383] {
    marker-width:6; marker-line-width:3; text-dy: -7;
  }
  [verbr_el>2528936] {
    marker-width:40; marker-line-width:10; text-dy:-41;
  }
}

#hoogsteenergie[zoom=13][el_round>7500] {
  marker-fill:@inner;
  marker-line-color:@outer;
  marker-allow-overlap:false;
  text-name:"[el_round]";
  text-face-name: @sans;
  text-fill: @text;
  text-allow-overlap:false;
  text-size:11;
  text-dy:-2;
  [verbr_el<=2528936] {
    marker-width:24; marker-line-width:8; text-dy:-25;
  }  
  [verbr_el<430383] {
    marker-width:18; marker-line-width:6; text-dy: -19;
  }
  [verbr_el<200217] {
    marker-width:10; marker-line-width:4; text-dy:-11;
  } 
  [verbr_el<40383] {
    marker-width:6; marker-line-width:3; text-dy: -7;
  }
  [verbr_el>2528936] {
    marker-width:40; marker-line-width:10; text-dy:-41;
  }
}

#hoogsteenergie[zoom>13] {
  marker-fill:@inner;
  marker-line-color:@outer;
  marker-allow-overlap:false;
  text-name:"[el_round]";
  text-face-name: @sans;
  text-fill: @text;
  text-allow-overlap:false;
  text-size:11;
  text-dy:-2;
  [verbr_el<=2528936] {
    marker-width:24; marker-line-width:8; text-dy:-25;
  }  
  [verbr_el<430383] {
    marker-width:18; marker-line-width:6; text-dy: -19;
  }
  [verbr_el<200217] {
    marker-width:10; marker-line-width:4; text-dy:-11;
  } 
  [verbr_el<40383] {
    marker-width:6; marker-line-width:3; text-dy: -7;
  }
  [verbr_el>2528936] {
    marker-width:40; marker-line-width:10; text-dy:-41;
  }
}


#nederland {
  line-color:#0AF;
  line-dasharray:1,1;
  line-width:0.5;
  line-opacity:0.5;
  [zoom=4] { line-width:0.6; }
  [zoom=5] { line-width:0.8; }
  [zoom=6] { line-width:1; }
  [zoom=7] { line-width:1.2; }
  [zoom=8] { line-width:1.4; }
  [zoom>8] { line-width:1.6; }
}

#nederland::glow-inner[zoom>7] {
  line-color:#225160;
  line-width:1.2;
  line-join:round;
  line-opacity:0.4;
}
#nederland::glow-innermiddle[zoom>7] {
  line-color:#225160;
  line-width:2.5;
  line-join:round;
  line-opacity:0.2;
}
#nederland::glow-outermiddle[zoom>7] {
  line-color:#225160;
  line-width:5;
  line-join:round;
  line-opacity:0.1;
}
#nederland::glow-outer[zoom>7] {
  line-color:#225160;
  line-width:5;
  line-join:round;
  line-opacity:0.05;
}