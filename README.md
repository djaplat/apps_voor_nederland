Source code bij Zoom in op Energie.
===================================

* convert.py: Python script om de .dx bestanden van ftp://ftp.ep-online.nl te converteren naar een .csv bestand

* Tilemill stylesheets: folder met Tilemill stylesheets voor de verschillende kaarten
panden, wegen, landgrenzen data is beschikbaar via: http://download.geofabrik.de/ of http://downloads.cloudmade.com/
gemeentedata is beschikbaar via: http://www.cbs.nl/nl-NL/menu/themas/dossiers/nederland-regionaal/publicaties/geografische-data/archief/2010/2010-wijk-en-buurtkaart-2009.htm

* TileStache: voorbeeld tilestache configuratie

* Website: de HTML en javascript van de website

![Requirements Status](https://requires.io/bitbucket/djaplat/apps_voor_nederland/requirements.png?branch=master)