# ======================================================================================================================= #
# De data bestaat uit een reeks zip files die elk een .dx bestand bevatten (in feite een xml bestand maar met een andere extensie)
# De data die van belang is staat in het Pandcertificaat-tag en bevat de volgende velden:
#
# PandVanMeting_postcode
# PandVanMeting_huisnummer
# PandVanMeting_huisnummer_toev
# PandVanMeting_gebouwcode
# PandVanMeting_opnamedatum
# PandVanMeting_energieprestatieindex
# PandVanMeting_energieverbruiktype
# PandVanMeting_energieverbruikmj
# PandVanMeting_energieverbruikelektriciteit
# PandVanMeting_energieverbruikgas
# PandVanMeting_energieverbruikwarmte
# PandVanMeting_energieverbruikco2
# PandVanMeting_opnameblauwdruk
# PandVanMeting_opnameobservatie
# PandVanMeting_opnameeigenaarinformatie
# PandVanMeting_energieklasse
# Meting_geldig_tot
# Afmeldnummer
# Pand_registratiedatum
# Pand_postcode
# Pand_huisnummer
# Pand_huisnummer_toev
# Pand_gebouwcode
# Pand_plaats
# Pand_cert_type
# ======================================================================================================================= #

# lxml moet geinstalleerd zijn en vindbaar zijn in de python path
#
# import sys
# sys.path.append('/<absolute>/<path>/<to>/python2.7/site-packages')


import csv
import os
import re
from lxml import etree

# pad naar de map met de dx bestanden folder (inclusief trailing slash)
sourceFolder = '<absolute>/<path>/<to>/<inputfolder>/'
# waar de output csv opgeslagen moet worden (inclusief trailing slash)
outputFolder = '<absolute>/<path>/<to>/<outputfolder>/'
# Naam van het outputbestand (zonder extensie)
outputFileName = "<output name>"
# Een lijst met velden die geconverteerd moeten worden (naam veld, naam veld in output, type veld)
conversionList = [
                    ["Pand_postcode", "postcode", "String"],
                    ["Pand_huisnummer", "nummer", "Integer"],
                    ["Pand_huisnummer_toev", "toevoeging", "String"],
                    ["Pand_plaats", "plaats", "String"],
                    ["PandVanMeting_energieklasse", "en_klasse", "String"],
                    ["PandVanMeting_opnamedatum", "opn_dat", "Integer"],
                    ["PandVanMeting_energieprestatieindex", "prest_idx", "Real"],
                    ["PandVanMeting_energieverbruiktype", "verbr_type", "String"],
                    ["PandVanMeting_energieverbruikmj", "verbr_mj", "Real"],
                    ["PandVanMeting_energieverbruikelektriciteit", "verbr_el", "Real"],
                    ["PandVanMeting_energieverbruikgas", "verbr_gas", "Real"],
                    ["PandVanMeting_energieverbruikwarmte", "verbr_w", "Real"],
                    ["PandVanMeting_energieverbruikco2", "verbr_co2", "Real"],
                    ["Meting_geldig_tot", "geld_tot", "Integer"],
                    ["Afmeldnummer", "afm_nr", "Integer"],
                    ["Pand_registratiedatum", "reg_dat", "Integer"],
                    ["Pand_gebouwcode", "geb_code", "String"],
                    ["Pand_cert_type", "cert_type", "String"]
                ]
# twee extra velden om een koppeling te kunnen maken met o.b.v postcode + huisnummer + toevoeging, of postcode + huisnummer, met bijvoorbeeld de BAG
extraFields = [
                ["join_field 1", "pcnrtoev", "String"],
                ["join_field 2", "pcnr", "String"]
            ]


def writeCsvtFile():
    """ maak een .csvt bestand dat de data types van de velden specificeert (kan handig zijn in QGis, zie: http://gothos.info/2011/04/joining-csv-files-in-qgis/)"""
    csvt = "%s%s.csvt" % (outputFolder, outputFileName)

    fields = [dtype[2] for dtype in conversionList]
    for ef in extraFields:
        fields.append(ef[2])

    with open(csvt, 'wb') as f:
        writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC, dialect=csv.excel)
        writer.writerow(fields)


def writeHeadersToCsv():
    """ Schrijf de eerste rij met header data naar het csv bestand"""
    csvFile = "%s%s.csv" % (outputFolder, outputFileName)

    headers = [h[1] for h in conversionList]
    for ef in extraFields:
        headers.append(ef[1])

    with open(csvFile, 'wb') as f:
        writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC, dialect=csv.excel)
        writer.writerow(headers)


def appendDataToCsv(sourceFolder, file, outfile):
    """ Voeg de rijen toe aan het csv bestand """
    dxFile = "%s%s" % (sourceFolder, file)
    context = etree.iterparse(dxFile, events=('end',), tag='Pandcertificaat')

    for event, certificaat in context:
        if (certificaat.find("PandVanMeting_postcode") is not None):
            row = []
            for c in conversionList:
                e = certificaat.find(c[0])
                try:
                    if e.text is not None:
                        e = re.sub(r'[^\w.]', '', e.text)
                        row.append(e)
                    else:
                        row.append('')
                except AttributeError, e:
                    row.append('')
            pcnrtoev = "%s_%s_%s" % (row[0], row[1], row[2])
            pcnr = "%s_%s" % (str(row[0]), str(row[1]))
            row.append(pcnrtoev.lower())
            row.append(pcnr.lower())

            print row

            csvFile = "%s%s.csv" % (outputFolder, outputFileName)
            with open(csvFile, 'a') as f:
                writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC, dialect=csv.excel)
                writer.writerow(row)

            certificaat.clear()
            while certificaat.getprevious() is not None:
                del certificaat.getparent()[0]


if __name__ == "__main__":
    writeHeadersToCsv()

    # loop door de source map en converteer alle bestanden naar 1 csv bestand
    for subdir, dirs, files in os.walk(sourceFolder):
        for file in files:
            if file.endswith('.dx'):
                appendDataToCsv(sourceFolder, file, outputFileName)

    writeCsvtFile()
