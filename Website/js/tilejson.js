var gemeentenjson = {
    "tilejson": "2.0.0",
    "name": "Energiegebruik Gemeenten",
    "description": "Energiegebruik gemeenten",
    "version": "1.0.0",
    "template": "{{#__location__}}{{/__location__}}{{#__teaser__}}<h2>{{{NAAM}}}</h2><p><img src=\"http://chart.googleapis.com/chart?chxl=0:|2009|2008|2006|2004&chxr=1,0,7500&chxt=x,y&chbh=r,10&chs=286x200&cht=bvg&chco=76A4FB&chds=0,7500&chd=t1:{{{el_2009}}},{{{el_2008}}},{{{el_2006}}},{{{el_2004}}}|3548,3513,3474,3408&chtt=Gemiddeld+verbruik+electriciteit&chm=D,FF9900,1,0,1&chf=a,s,000000E6|bg,s,000000&chxs=0,FFFFFF,10.833,0,l,FFFFFF|1,FFFFFF,11.5,0,l,00000000&chts=FFFFFF,11.5\" /></p><p><img src=\"http://chart.googleapis.com/chart?chxl=0:|2009|2008|2006|2004&chxr=1,0,4200&chxt=x,y&chbh=r,10&chs=286x200&cht=bvg&chco=76A4FB&chds=0,4200&chd=t1:{{{gas_2009}}},{{{gas_2008}}},{{{gas_2006}}},{{{gas_2004}}}|1830,1846,1836,1872&chtt=Gemiddeld+verbruik+gas&chm=D,FF9900,1,0,1&chf=a,s,000000E6|bg,s,000000&chxs=0,FFFFFF,10.833,0,l,FFFFFF|1,FFFFFF,11.5,0,l,00000000&chts=FFFFFF,11.5\" /></p>{{/__teaser__}}{{#__full__}}{{/__full__}}",
    "legend": "energiegebruik_gemeenten",
    "scheme": "xyz",
    "tiles": [
        "http://127.0.0.1:1234/energie_gemeenten/{z}/{x}/{y}.png"
    ],
    "grids": [
        "http://127.0.0.1:1234/gemeenten_json/{z}/{x}/{y}.json"
    ],
    "minzoom": 8,
    "maxzoom": 15,
    "bounds": [3.3, 53.6, 7.3, 50.7],
    "center": [52.32017, 6.36804, 8]
};

var gebouwenjson = {
    "tilejson": "2.0.0",
    "name": "Energielabels Gebouwen",
    "description": "Energielabels gebouwen",
    "version": "1.0.0",
    "center": [52.32017, 6.36804, 8],
    "legend": "energielabels",
    "scheme": "xyz",
    "tiles": [
        "http://127.0.0.1:1234/apps_voor_nederland/{z}/{x}/{y}.png"
    ],
    "minzoom": 8,
    "maxzoom": 15,
    "bounds": [3.3, 53.6, 7.3, 50.7]
};

var hosjson = {
    "tilejson": "2.0.0",
    "name": "Hall of Shame",
    "description": "De grootste energieverbruikers van Nederland",
    "version": "1.0.0",
    "center": [52.32017, 6.36804, 8],
    "legend": "hall_of_shame",
    "scheme": "xyz",
    "tiles": [
        "http://127.0.0.1:1234/apps_voor_nederland/hos/{z}/{x}/{y}.png"
    ],
    "minzoom": 8,
    "maxzoom": 15,
    "bounds": [3.3, 53.6, 7.3, 50.7]
};